import pydicom
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
import os
import math


############ change these parameters
OCR1A = 468
input_data_filepath = '../phantom data 2/1.dcm'
save_filepath = './images_phantom2-1-split-reverse' # folder to save images in
subsample = 10 # save every Nth axial slice
############

degrees = 1.8  # degrees stepper motor rotates every step
microstep = 8  # is the stepper motor using microsteps? if so, what size?

ds = pydicom.dcmread(input_data_filepath) # read .dcm file
data = ds.pixel_array # get pixel data

if not os.path.exists(save_filepath):
    os.makedirs(save_filepath)


"""
interrupt frequency (Hz) = (Arduino clock speed 16,000,000Hz) / (prescaler * (compare match register + 1))
the +1 is in there because the compare match register is zero indexed. Divide again by 2 because it takes 2 interrupts for a single step
"""
freq = 16e6 / (256 * (OCR1A + 1)) / 2
print(freq)

height = ds.Rows  # get height of the frames
width = ds.Columns  # get width of the frames
time_vector = ds.FrameTimeVector # get time intervals between each frame
# print(height)
# print(width)

intensity_data = np.dot(data[..., :3], [0.299, 0.587, 0.114])  # convert to grayscale before concatenating to array
print(intensity_data.shape)

total_time = 0
times = []
for time in time_vector: # calculate time stamps
    total_time += time
    times.append(total_time)
# print(times)
# print(len(times))

angles = np.array(times) / 1000 * freq * degrees / microstep * np.pi / 180 # convert time stamps to angles
thetas_rad = angles[angles < 2*np.pi]
# thetas_rad = np.flip(thetas_rad)
print(thetas_rad)

raxis = np.linspace(math.floor(-width/2), 0, math.ceil(width/2))  # radial positions
# print(raxis)
# raxis = range(-width//2, 0)
# print(raxis)
grid_x, grid_y = np.mgrid[-width / 2:width / 2, -width / 2:width / 2]  # create grid to interpolate

z_range = range(int(height * .2), int(height * .8), subsample) # reconstruct only the middle 60% of the saved image (Clarius pads image with black borders)
for slice_index, zi in enumerate(z_range):
    xy_list = np.zeros(
        [len(thetas_rad) * len(raxis), 2])  # (x, y) cartesian coordinates for all the points in an axial slice
    v_list = np.zeros(len(thetas_rad) * len(raxis))  # pixel intensities (0-255) for all the points in an axial slice
    count = 0
    for ti, theta in enumerate(thetas_rad):  # convert points in each axial slice into cartesian coordinates
        for ri, r in enumerate(raxis):
            x = r * np.cos(theta)  #
            y = r * np.sin(theta)
            xy_list[count, :] = [x, y]
            v_list[count] = intensity_data[ti, zi, ri]
            count = count + 1
    v_interp = griddata(xy_list, v_list, (grid_x, grid_y), method='linear')  # interpolate into a grid

    plt.figure(figsize=(width / 100, width / 100), dpi=150)  # make sure figure resolution > interpolation grid
    plt.axis('off')  # remove axes from image
    plt.imshow(v_interp, cmap='gray', vmin=0, vmax=255)
    name = f'slice{len(z_range) - slice_index:03d}'  # do (height - zi) to save images in a down to up order
    filename = os.path.join(save_filepath, name)
    plt.savefig(filename, bbox_inches='tight')
    plt.close()
    print(f'{len(z_range) - slice_index}/{len(z_range)}')
