//Declare pin functions on Arduino
#define stp 2
#define dir 3
#define MS1 4
#define MS2 5
#define EN  6

//Declare variables for functions
char user_input;
int x;
int y;
int state;

int drive_state = 0;

void setup() {
  pinMode(stp, OUTPUT);
  pinMode(dir, OUTPUT);
  pinMode(MS1, OUTPUT);
  pinMode(MS2, OUTPUT);
  pinMode(EN, OUTPUT);
  resetEDPins(); //Set step, direction, microstep and enable pins to default states
  Serial.begin(9600); //Open Serial connection for debugging
  Serial.println("Begin motor control");
  Serial.println();
  //Print function list for user selection
  Serial.println("Enter number for control option:");
  Serial.println("1. Turn at default microstep mode.");
  Serial.println("2. Reverse direction at default microstep mode.");
  Serial.println("3. Turn at 1/8th microstep mode.");
  Serial.println("4. Reverse direction at 1/8th microstep mode.");
  Serial.println("5. Stop.");

  //https://www.instructables.com/id/Arduino-Timer-Interrupts/

  cli();//stop interrupts

//set timer1 interrupt at 1Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  //OCR1A = (16*10^6 / (stepFrequencyInHz*256) - 1) / 2     (must be <65536, divide by 2 is because it takes 2 interrupts for 1 step to occur) 
  //OCR1A = 312; // 8 seconds/180deg
  //OCR1A = 585; // 15 seconds/180deg
  //OCR1A = 546; // 14 seconds/180deg
  OCR1A = 468; //12 second/180deg
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 bit for 256 prescaler
  TCCR1B |= (1 << CS12);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();//allow interrupts
}

//Main loop
void loop() {
  while(Serial.available()){
      user_input = Serial.read(); //Read user input and trigger appropriate function
      if (user_input =='1')
      {
         StepForwardDefault();
         digitalWrite(EN, LOW); //Pull enable pin low to allow motor control
      }
      else if(user_input =='2')
      {
        ReverseStepDefault();
         digitalWrite(EN, LOW); //Pull enable pin low to allow motor control

      }
      else if(user_input =='3')
      {
        SmallStepMode();
         digitalWrite(EN, LOW); //Pull enable pin low to allow motor control

      }
      else if(user_input =='4')
      {
        ReverseSmallStep();
         digitalWrite(EN, LOW); //Pull enable pin low to allow motor control

      }
      else if(user_input=='5'){
         digitalWrite(EN, HIGH); //Pull enable pin low to allow motor control
          Serial.println("Stopped");

      }


  }
}

//Interrupt code
ISR(TIMER1_COMPA_vect){  //change the 0 to 1 for timer1 and 2 for timer2
   //interrupt commands here
   if (drive_state==0){
    digitalWrite(stp,HIGH); //Trigger one step forward
    drive_state = 1;
   }
   else{
    digitalWrite(stp,LOW);
    drive_state = 0;
   }
   
}

//Reset Easy Driver pins to default states
void resetEDPins()
{
  digitalWrite(stp, LOW);
  digitalWrite(dir, LOW);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(EN, HIGH);
}

//Default microstep mode function
void StepForwardDefault()
{
  Serial.println("Moving forward at default step mode.");
  digitalWrite(dir, LOW); //Pull direction pin low to move "forward"
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
}

//Reverse default microstep mode function
void ReverseStepDefault()
{
  Serial.println("Moving in reverse at default step mode.");
  digitalWrite(dir, HIGH); //Pull direction pin high to move in "reverse"
    digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
}

// 1/8th microstep foward mode function
void SmallStepMode()
{
  Serial.println("Stepping at 1/8th microstep mode.");
  digitalWrite(dir, LOW); //Pull direction pin low to move "forward"
  digitalWrite(MS1, HIGH); //Pull MS1, and MS2 high to set logic to 1/8th microstep resolution
  digitalWrite(MS2, HIGH);
}

//Forward/reverse stepping function
void ReverseSmallStep()
{
  Serial.println("Reverse stepping at 1/8th microstep mode.");
  digitalWrite(dir, HIGH); //Pull direction pin low to move "forward"
  digitalWrite(MS1, HIGH); //Pull MS1, and MS2 high to set logic to 1/8th microstep resolution
  digitalWrite(MS2, HIGH);
}
